<?php

Route::group(['middleware' => 'guest'], function () {

    Route::get('/', 'Auth\AuthenticationController@showLoginForm')->name('authorization');
    Route::post('auth', 'Auth\AuthenticationController@login');

    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

});

Route::group(['middleware' => 'auth'], function () {

    Route::get('profile', 'UserController@profile')->name('profile');
    Route::get('profile/edit', 'UserController@profileEdit')->name('edit-profile');
    Route::get('profile/{user}', 'UserController@otherUserProfile')->middleware('not-my-profile');
    Route::put('profile/{user}', 'UserController@profileUpdate');

    Route::get('logout', 'Auth\AuthenticationController@logout')->name('logout');
});