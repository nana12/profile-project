@extends('layouts.mail-layout')

@section('content')
    <p>Спасибо за регистрацию!</p>
    <p>Используйте свой email <strong>{{$email}}</strong> для входа.</p>
@endsection