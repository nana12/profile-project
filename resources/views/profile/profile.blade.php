@extends('layouts.layout')

@section('content')

    <main class="login-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="card-group">

                        <div class="card">
                            <div class="card-header">Мой профиль</div>

                            <div class="card-body">
                                <p class="card-text"><strong>Имя: </strong>@if($user->name) {{$user->name}}@else Не
                                    указано @endif</p>
                                <p class="card-text"><strong>О
                                        себе: </strong>@if($user->about) {!! $user->about !!}@else Не указано @endif</p>
                                <a href="{{url('/profile/edit')}}" class="btn btn-light" href="">Редактировать</a>
                            </div>
                        </div>

                        @can('viewUsersList', \App\Modules\User\Models\User::class)
                            <div class="card">
                                <div class="card-header">Другие пользователи</div>

                                <div class="card-body">
                                    @forelse($userList as $otherUser)
                                        <div class="list-group">
                                            <a href="{{url('/profile/' . $otherUser->id)}}" class="list-group-item list-group-item-action">
                                                <div class="d-flex w-100 justify-content-between">
                                                    <h5 class="mb-1">{{$otherUser->name}}</h5>
                                                </div>
                                                <p class="mb-1">{{$otherUser->about}}</p>
                                            </a>
                                       </div>
                                    @empty
                                    @endforelse
                                </div>
                            </div>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection