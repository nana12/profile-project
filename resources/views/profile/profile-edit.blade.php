@extends('layouts.layout')

@section('content')

    <main class="login-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">Редактирование профиля</div>
                        <div class="card-body">

                            <form action="/profile/{{$user->id}}" method="post">

                                @method('put')
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Имя</label>
                                    <div class="col-md-6">
                                        <input type="text" id="name" class="form-control" name="name"
                                               value="{{$user->name}}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="gender" class="col-md-4 col-form-label text-md-right">Пол</label>
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="gender"
                                                   value="F" @if($user->gender === 'F') checked @endif>
                                            <label class="form-check-label" for="gender">Женщина</label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="gender" id="gender"
                                                   value="M" @if($user->gender === 'M') checked @endif>
                                            <label class="form-check-label" for="gender">Мужчина</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="about" class="col-md-4 col-form-label text-md-right">О себе</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" name="about" id="about">{{$user->about}}</textarea>
                                    </div>
                                </div>


                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Сохранить
                                    </button>
                                    <a href="{{url()->previous()}}" class="btn btn-light">
                                        Отменить
                                    </a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection