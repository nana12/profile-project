@extends('layouts.layout')

@section('content')

    <main class="login-form">
        <div class="cotainer">
            <div class="row justify-content-center">
                <div class="col-md-8">

                    <div class="card-group">

                        <div class="card">
                            <div class="card-header">Профиль пользователя</div>

                            <div class="card-body">
                                <p class="card-text"><strong>Имя: </strong>@if($user->name) {{$user->name}}@else Не
                                    указано @endif</p>
                                <p class="card-text"><strong>О
                                        себе: </strong>@if($user->about) {!! $user->about !!}@else Не указано @endif</p>

                                <a href="{{url(route('profile'))}}" class="btn btn-light">Назад в профиль</a>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection