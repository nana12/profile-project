<?php


namespace App\Modules\User\Services;


use App\Modules\AbstractService;
use App\Modules\User\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;

class UserService extends AbstractService
{
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function list(User $exceptUser = null)
    {
        $builder = $this->model
            ->whereNotNull(['name', 'about']);

        if ($exceptUser) {
            $builder->where('id', '<>', $exceptUser->id);
        }

        return $builder->get();
    }


    public function update(User $updatingUser, array $data)
    {
        $user = Auth::user();

        if ($user->can('update', $updatingUser)) {
            $user->update([
                'name' => $data['name'],
                'gender' => $data['gender'],
                'about' => $data['about']
            ]);

            return $user;
        }

        throw new AuthorizationException('У вас нет прав на доступ к данному ресурсу.');
    }

    public function getUserProfile(User $user)
    {
        $this->authorize('viewUsersList', User::class);

    }
}