<?php

namespace App\Modules\User\Policies;

use App\Modules\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function update(User $authUser, User $updatingUser)
    {
        return $authUser->id === $updatingUser->id;
    }

    public function viewUsersList(User $user)
    {
        return !empty($user->name) && !empty($user->about) && !empty($user->gender);
    }
}
