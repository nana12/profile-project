<?php


namespace App\Modules;


use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AbstractService
{
    use AuthorizesRequests;
}