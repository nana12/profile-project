<?php

namespace App\Http\Middleware;

use App\Modules\User\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfMyProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');

        if(Auth::user()->id === $user->id) {
            return redirect(route('profile'));
        }

        return $next($request);
    }
}
