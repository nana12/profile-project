<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function sendJsonResponse($data = null, $status = 200)
    {
        return response()->json($data, $status);
    }

    protected function sendViewResponse($view, $data = null)
    {
        return response()->view($view, $data);
    }

    protected function sendRedirectToResponse($path, $msgKey = null, $msgVal = null)
    {
        $response = Redirect::to($path);

        return $msgKey !== null && $msgVal !== null
            ? $response->with($msgKey, $msgVal)
            : $response;
    }

    protected function sendRedirectToRouteResponse($route,  $msgKey = null, $msgVal = null)
    {
        $response = Redirect::route($route);

        return $msgKey !== null && $msgVal !== null
            ? $response->with($msgKey, $msgVal)
            : $response;
    }

    protected function sendRedirectBackResponse($msgKey = null, $msgVal = null)
    {
        $response = Redirect::back();

        return $msgKey !== null && $msgVal !== null
            ? $response->with($msgKey, $msgVal)
            : $response;
    }
}
