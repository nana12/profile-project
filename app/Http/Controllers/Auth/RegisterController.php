<?php

namespace App\Http\Controllers\Auth;

use App\Modules\User\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\{
    RedirectsUsers, RegistersUsers
};
use Illuminate\Support\Facades\{
    Hash, Validator
};

class RegisterController extends Controller
{
    use RedirectsUsers, RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('edit-profile');
    }

    /**
    */
    public function showRegisterForm()
    {
        return view('auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:5'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return App\Modules\User\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
