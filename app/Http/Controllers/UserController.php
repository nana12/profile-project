<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Modules\User\{
    Models\User,
    Services\UserService
};
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Access\AuthorizationException;

class UserController extends Controller
{
    /**
     * View my profile
     * @param UserService $service
     * @return Response
     */
    public function profile(UserService $service)
    {
        $user = Auth::user();

        return $this->sendViewResponse('profile.profile', [
            'user' => $user,
            'userList' => $service->list($user)
        ]);
    }

    /**
     * Edit profile view
     * @return Response
     */
    public function profileEdit()
    {
        return $this->sendViewResponse('profile.profile-edit', [
            'user' => Auth::user()
        ]);
    }


    public function profileUpdate(User $user, UserRequest $request, UserService $service)
    {
        if (!$service->update($user, $request->post())) {
            return $this->sendRedirectBackResponse('error', 'Что-то пошло не так.');
        }

        return $this->sendRedirectToRouteResponse('profile');
    }


    public function otherUserProfile(User $user, UserService $service)
    {
        try {
            $service->getUserProfile($user);
        } catch (AuthorizationException $e) {

            return $this->sendRedirectToRouteResponse('profile',
                'error', 'Заполните всю информацию о себе');
        }

        return $this->sendViewResponse('profile.profile-other-user', compact('user'));
    }
}
